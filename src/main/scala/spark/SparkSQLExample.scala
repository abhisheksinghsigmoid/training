package spark

import org.apache.spark.sql.SparkSession

object runBasicDataFrameExample extends App {

  val spark = SparkSession
    .builder()
    .appName("Spark SQL basic example")
    .getOrCreate()

  val df = spark.read.json("/home/spark/articles/*.json")

  df.show()
  df.printSchema()
  df.select("id", "name", "salary", "deptid", "address" ,"city", "state", "country").show()
  var total:Long=df.count();

  println("total empolyees are: "+total);


  df.groupBy("deptid").count().show();

  df.groupBy("deptid").avg("salary").show();
  df.groupBy("city").avg("salary").show();
  df.groupBy("country").avg("salary").show();
}