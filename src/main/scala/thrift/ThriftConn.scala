package thrift

import org.apache.thrift.transport.{TSocket, TTransport}
object ThriftConn {

  def getClient(): TTransport = {
    val host = "localhost"
    val port = 8080

    val socket = new TSocket(host, port)
    socket.setConnectTimeout(2000)
    val transport: TTransport = socket
    transport

  }


}