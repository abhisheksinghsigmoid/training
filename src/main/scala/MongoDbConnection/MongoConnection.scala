package MongoDbConnection

import MongoDbConnection.Helpers._
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._

object MongoConnection {

  /**
    * Run this main method to see the output of this quick example.
    *
    * @param args takes an optional single argument for the connection string
    * @throws Throwable if an operation fails
    */
  def main(args: Array[String]): Unit = {

    val mongoClient: MongoClient = if (args.isEmpty) MongoClient() else MongoClient(args.head)
    val database: MongoDatabase = mongoClient.getDatabase("mydb")
    val collection: MongoCollection[Document] = database.getCollection("EmpDetails")

    collection.updateMany(exists("Salary"),inc("Salary", 1000)).printHeadResult("Update Result: ")

    collection.find().sort(descending("Salary")).first().printHeadResult()

    // release resources
    mongoClient.close()
  }

}