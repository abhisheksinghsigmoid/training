package MySQLConnection

import java.sql._

object ScalaJdbcConnectSelect extends App {
  // connect to the database named "mysql" on port 8889 of localhost
  val url = "jdbc:mysql://localhost:8889/mysql"
  val driver = "com.mysql.jdbc.Driver"
  val username = "root"
  val password = "jaanjaoge"
  var connection:Connection = _


  try {
    Class.forName(driver)
    connection = DriverManager.getConnection(url, username, password)
    val statement = connection.createStatement
    val updateQuery= "UPDATE EmpDetails SET Salary = Salary * 1.15"
    val preparedStmt: PreparedStatement = connection.prepareStatement(updateQuery);
    preparedStmt.executeUpdate();
    val rs = statement.executeQuery("SELECT DeptID, MAX(Salary),EmpName FROM EmpDetails GROUP BY DeptID")
    while (rs.next) {
      val Department = rs.getString("DeptId")
      val Salary = rs.getInt("MAX(Salary)")
      val Employeename = rs.getString("EmpName")
      println("DeptID = %s, MAX(Salary) = %d, EmpName = %s".format(Department,Salary,Employeename))
    }
  } catch {
    case e: Exception => e.printStackTrace
  }


  connection.close
}


