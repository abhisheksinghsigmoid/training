organization := "com.typesafe.akka.samples"
name := "akka-sample"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.0.0"
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.0.0"
libraryDependencies += "org.apache.thrift" % "libthrift" % "0.9.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.22",
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.6.0",
  "org.slf4j" % "slf4j-simple" % "1.6.4",
  "org.apache.thrift" % "libthrift" % "0.9.2"
)

licenses := Seq(("CC0", url("http://creativecommons.org/publicdomain/zero/1.0")))
